let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log ("Results of square bracket notation: ")
console.log (trainer["pokemon"]);

console.log("Result of talk method:")
trainer.talk();

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level * 1.5;

    this.tackle = function(target) {
        console.log(this.name + " attacked " + target.name);
        target.health -= this.attack;
        console.log(target.name + "'s health is reduced to " + target.health);
        if (target.health < 1) {
            target.faint();
        }
    }

    this.faint = function() {
        console.log(this.name + " fainted.");
    }
}

let pokeball1 = new Pokemon("Pikachu", 12);
let pokeball2 = new Pokemon("Geodude", 8);
let pokeball3 = new Pokemon("Mewtwo", 100);

console.log(pokeball1);
console.log(pokeball2);
console.log(pokeball3);

pokeball2.tackle(pokeball1);

console.log(pokeball1);

pokeball3.tackle(pokeball2);

console.log(pokeball2);